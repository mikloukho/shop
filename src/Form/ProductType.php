<?php

namespace App\Form;

use App\Entity\Product;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Validator\Constraints\NotBlank;
use Symfony\Component\OptionsResolver\OptionsResolver;

class ProductType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('title', options: [
                'constraints' => [
                    new NotBlank(message: 'Field is required')
                ]
            ])
            ->add('price', options: [
                'constraints' => [
                    new NotBlank(message: 'Field is required')
                ],
                'scale' => 2
            ])
            ->add('quantity', options: [
                'constraints' => [
                    new NotBlank(message: 'Field is required')
                ],
                'attr' => ['min' => 0]
            ])
            ->add('description', options: [
                'required' => false
            ])
            ->add('isPublished', options: [

            ])
            ->add('isDeleted', options: [

            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => Product::class,
        ]);
    }
}
